package testDemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBC_Demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection a = DriverManager.getConnection("jdbc:mysql://localhost:3306/greenfield_training","root","");
			Statement b = a.createStatement();
			ResultSet c = b.executeQuery("select *from employee");
			while(c.next()) {
				System.out.println(c.getString(1) + " "+ c.getString(2)+" "+c.getInt(3));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
